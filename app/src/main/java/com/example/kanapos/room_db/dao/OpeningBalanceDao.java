package com.example.kanapos.room_db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.pojo.OpeningBalancePojo;

@Dao
public interface OpeningBalanceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(OpeningBalancePojo openingBalance);


    @Query("SELECT * FROM " + DBConfig.table_opening_balance + " WHERE date_at = :date_at")
    OpeningBalancePojo fetchByDate(String date_at);


    @Update
    void update(OpeningBalancePojo openingBalance);


    @Delete
    void delete(OpeningBalancePojo openingBalance);
}
