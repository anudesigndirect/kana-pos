package com.example.kanapos.room_db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.pojo.StaffPojo;

import java.util.List;

@Dao
public interface StaffDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insert(List<StaffPojo> staff);


    @Query("SELECT * FROM " + DBConfig.table_staff)
    List<StaffPojo> getAllStaff();

    @Query("SELECT * FROM " + DBConfig.table_staff + " WHERE logged_status = 1")
    StaffPojo getLoggedStaff();


    @Update
    int update(StaffPojo staff);


    @Delete
    void delete(StaffPojo staff);
}
