package com.example.kanapos.room_db;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.example.kanapos.room_db.dao.CategoryDao;
import com.example.kanapos.room_db.dao.CompanyDao;
import com.example.kanapos.room_db.dao.LocationDao;
import com.example.kanapos.room_db.dao.OpeningBalanceDao;
import com.example.kanapos.room_db.dao.ProductDao;
import com.example.kanapos.room_db.dao.RoomDao;
import com.example.kanapos.room_db.dao.StaffDao;
import com.example.kanapos.room_db.pojo.ButtonColorPojo;
import com.example.kanapos.room_db.pojo.CategoryPojo;
import com.example.kanapos.room_db.pojo.CompanyPojo;
import com.example.kanapos.room_db.pojo.LocationPojo;
import com.example.kanapos.room_db.pojo.OpeningBalancePojo;
import com.example.kanapos.room_db.pojo.OrderBillTypesPojo;
import com.example.kanapos.room_db.pojo.OrderMethodsPojo;
import com.example.kanapos.room_db.pojo.OrderStatusPojo;
import com.example.kanapos.room_db.pojo.ProductPojo;
import com.example.kanapos.room_db.pojo.ProductPositionsPojo;
import com.example.kanapos.room_db.pojo.StaffPojo;

@Database(entities = {
        LocationPojo.class,
        CompanyPojo.class,
        StaffPojo.class,
        OpeningBalancePojo.class,
        CategoryPojo.class,
        ProductPojo.class,
        ButtonColorPojo.class,
        ProductPositionsPojo.class,
        OrderBillTypesPojo.class,
        OrderMethodsPojo.class,
        OrderStatusPojo.class
}, version = 1, exportSchema = false)
public abstract class DBConfig extends RoomDatabase {

    public static final String table_company = "company";

    public static final String table_location = "location";

    public static final String table_staff = "staff";

    public static final String table_opening_balance = "opening_balance";

    public static final String table_categories = "categories";

    public static final String table_products = "products";

    public static final String button_colors = "button_colors";

    public static final String product_positions = "product_positions";

    public static final String order_bill_types = "order_bill_types";

    public static final String order_methods = "order_methods";

    public static final String order_status = "order_status";

    public static final String order_type = "order_type";


    public abstract LocationDao locationDao();

    public abstract CompanyDao companyDao();

    public abstract StaffDao staffDao();

    public abstract OpeningBalanceDao openingBalanceDao();

    public abstract CategoryDao categoryDao();

    public abstract ProductDao productDao();

    public abstract RoomDao roomDao();

    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }
}
