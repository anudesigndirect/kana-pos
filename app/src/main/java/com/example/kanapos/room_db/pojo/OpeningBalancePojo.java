package com.example.kanapos.room_db.pojo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.kanapos.room_db.DBConfig;

@Entity(tableName = DBConfig.table_opening_balance)
public class OpeningBalancePojo {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private double amount;

    private String date_at;

    public OpeningBalancePojo(double amount, String date_at){
        this.amount=amount;
        this.date_at=date_at;
    }


    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDate_at() {
        return date_at;
    }

    public void setDate_at(String date_at) {
        this.date_at = date_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
