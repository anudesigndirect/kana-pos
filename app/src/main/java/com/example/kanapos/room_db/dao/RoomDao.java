package com.example.kanapos.room_db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.pojo.ButtonColorPojo;
import com.example.kanapos.room_db.pojo.OrderBillTypesPojo;
import com.example.kanapos.room_db.pojo.OrderMethodsPojo;
import com.example.kanapos.room_db.pojo.OrderStatusPojo;
import com.example.kanapos.room_db.pojo.ProductPojo;
import com.example.kanapos.room_db.pojo.ProductPositionsPojo;
import com.example.kanapos.room_db.pojo.StaffPojo;

import java.util.List;

@Dao
public interface RoomDao {

    ///////////////////BUTTON COLORS//////////////////////////////////////////
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertButtonColor(List<ButtonColorPojo> staff);


    @Query("SELECT * FROM " + DBConfig.button_colors)
    List<ButtonColorPojo> getAllButtonColor();

    @Query("SELECT * FROM " + DBConfig.button_colors + " WHERE id = :id")
    ButtonColorPojo getAllButtonColorById(int id);


    @Update
    int updateButtonColor(ButtonColorPojo data);


    @Delete
    void deleteButtonColor(ButtonColorPojo data);


    ///////////////////BUTTON COLORS//////////////////////////////////////////
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertProductPositions(List<ProductPositionsPojo> list);

    // get favorites - Favorites are items saved on product position table.

    @Query("SELECT * FROM " + DBConfig.table_products + " AS t INNER JOIN product_positions AS p ON p.productId = t.id")
    List<ProductPojo> getFavorites();

    /////////////////ORDER BILL TYPE//////////////////////////////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertOrderBillType(List<OrderBillTypesPojo> list);

    /////////////////ORDER METHODS//////////////////////////////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertOrderMethods(List<OrderMethodsPojo> list);
    /////////////////ORDER STATUS//////////////////////////////////////////

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertOrderStatus(List<OrderStatusPojo> list);

}
