package com.example.kanapos.room_db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.pojo.LocationPojo;

import java.util.List;

@Dao
public interface LocationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(LocationPojo locationModel);


    @Query("SELECT * FROM "+ DBConfig.table_location )
    List<LocationPojo> fetchLocation();


    @Update
    void update(LocationPojo locationModel);


    @Delete
    void delete(LocationPojo locationModel);
}
