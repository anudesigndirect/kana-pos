package com.example.kanapos.room_db;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.room.Room;

import com.example.kanapos.room_db.pojo.ButtonColorPojo;
import com.example.kanapos.room_db.pojo.CategoryPojo;
import com.example.kanapos.room_db.pojo.CompanyPojo;
import com.example.kanapos.room_db.pojo.LocationPojo;
import com.example.kanapos.room_db.pojo.OpeningBalancePojo;
import com.example.kanapos.room_db.pojo.OrderBillTypesPojo;
import com.example.kanapos.room_db.pojo.OrderMethodsPojo;
import com.example.kanapos.room_db.pojo.ProductPojo;
import com.example.kanapos.room_db.pojo.ProductPositionsPojo;
import com.example.kanapos.room_db.pojo.StaffPojo;

import java.util.List;

/*Singleton class*/
public class DBRepository {

    private static DBRepository dbRepository;

    public static String DB_NAME = "db_kana_pos";

    static DBConfig DBConfig;

    private DBRepository() {

    }

    public static DBRepository getInstance(Context context) {

        DBConfig = Room.databaseBuilder(context, DBConfig.class, DB_NAME).build();
        if (dbRepository == null) {
            dbRepository = new DBRepository();
        }
        return dbRepository;
    }

    public void insertLocation(final LocationPojo locationModel) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DBConfig.locationDao().insert(locationModel);
                return null;
            }
        }.execute();
    }

    public void insertCompany(final CompanyPojo companyModel) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DBConfig.companyDao().insert(companyModel);
                return null;
            }
        }.execute();
    }

    /////////////////////////////STAFF///////////////////////////////////////////////////////////////

    public void insertStaff(final List<StaffPojo> staff) {
        new AsyncTask<Void, Void, Long[]>() {
            @Override
            protected Long[] doInBackground(Void... voids) {
                return DBConfig.staffDao().insert(staff);
            }

            @Override
            protected void onPostExecute(Long[] longs) {
                super.onPostExecute(longs);

                Log.d("DataGet", longs[0] + " sd");
            }
        }.execute();
    }

    public void updateStaff(final StaffPojo staff) {
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... voids) {
                return DBConfig.staffDao().update(staff);
            }

            @Override
            protected void onPostExecute(Integer longs) {
                super.onPostExecute(longs);

                Log.d("DataGet", longs + " sd");
            }
        }.execute();
    }

    public List<StaffPojo> getAllStaff() {
        return DBConfig.staffDao().getAllStaff();
    }


    public StaffPojo getLoggedStaff() {
        return DBConfig.staffDao().getLoggedStaff();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /*Opening balance*/
    public void insertOpeningBalance(final OpeningBalancePojo openingBalance) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DBConfig.openingBalanceDao().insert(openingBalance);
                return null;
            }
        }.execute();
    }


    public OpeningBalancePojo getOpeningBalancebyDate(String date_at) {
        return DBConfig.openingBalanceDao().fetchByDate(date_at);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /*CATEGORIES*/
    public void insertCategories(final List<CategoryPojo> category) {
        new AsyncTask<Void, Void, Long[]>() {
            @Override
            protected Long[] doInBackground(Void... voids) {
                return DBConfig.categoryDao().insert(category);
            }

            @Override
            protected void onPostExecute(Long[] longs) {
                super.onPostExecute(longs);

                Log.d("DataGet", longs[0] + " sd");
            }
        }.execute();
    }

    public List<CategoryPojo> getAllCategories() {
        return DBConfig.categoryDao().getAllCategories();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////

    /*PRODUCTS*/
    public void insertProducts(final List<ProductPojo> product) {
        new AsyncTask<Void, Void, Long[]>() {
            @Override
            protected Long[] doInBackground(Void... voids) {
                return DBConfig.categoryDao().insertProduct(product);
            }

            @Override
            protected void onPostExecute(Long[] longs) {
                super.onPostExecute(longs);

                Log.d("DataGet", longs[0] + " sd");
            }
        }.execute();
    }

    public List<ProductPojo> getAllProducts() {
        return DBConfig.productDao().getAllData();
    }
    public List<ProductPojo> getProductsByCategory(String value) {
        return DBConfig.productDao().getDataByCondition(value);
    }

    //////////////Button colors/////////////////////////////////////////////////////////////////////////////////////
    /*PRODUCTS*/
    public void insertButtonColors(final List<ButtonColorPojo> list) {
        new AsyncTask<Void, Void, Long[]>() {
            @Override
            protected Long[] doInBackground(Void... voids) {
                return DBConfig.roomDao().insertButtonColor(list);
            }

            @Override
            protected void onPostExecute(Long[] longs) {
                super.onPostExecute(longs);

            }
        }.execute();
    }

    public List<ButtonColorPojo> getButtonColors() {
        return DBConfig.roomDao().getAllButtonColor();
    }
    public ButtonColorPojo getButtonColorByID(int id) {
        return DBConfig.roomDao().getAllButtonColorById(id);
    }

    ////////////////////////Product positions/////////////////////////////////////////////
    public void insertProductPositions(final List<ProductPositionsPojo> list) {
        new AsyncTask<Void, Void, Long[]>() {
            @Override
            protected Long[] doInBackground(Void... voids) {
                return DBConfig.roomDao().insertProductPositions(list);
            }

            @Override
            protected void onPostExecute(Long[] longs) {
                super.onPostExecute(longs);

            }
        }.execute();
    }

    /*Get favorites*/
    public List<ProductPojo> getFavorites() {

        return DBConfig.roomDao().getFavorites();

    }


    /////////////////Order bill type//////////////////////////////////////////////////////////////

    public void insertOrderBillType(final List<OrderBillTypesPojo> list) {
        new AsyncTask<Void, Void, Long[]>() {
            @Override
            protected Long[] doInBackground(Void... voids) {
                return DBConfig.roomDao().insertOrderBillType(list);
            }

            @Override
            protected void onPostExecute(Long[] longs) {
                super.onPostExecute(longs);

            }
        }.execute();
    }

    /////////////////Order methods//////////////////////////////////////////////////////////////

    public void insertOrderMethods(final List<OrderMethodsPojo> list) {
        new AsyncTask<Void, Void, Long[]>() {
            @Override
            protected Long[] doInBackground(Void... voids) {
                return DBConfig.roomDao().insertOrderMethods(list);
            }

            @Override
            protected void onPostExecute(Long[] longs) {
                super.onPostExecute(longs);

            }
        }.execute();
    }


}
