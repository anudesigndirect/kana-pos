package com.example.kanapos.room_db.pojo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.kanapos.room_db.DBConfig;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = DBConfig.table_staff)
public class StaffPojo {

    @PrimaryKey
    @SerializedName("id")
    private Integer id;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("display_name")
    private String displayName;

    @SerializedName("pin")
    private String pin;

    @SerializedName("phone")
    private String phone;

    @SerializedName("forall_loc")
    private String forallLoc;

    @SerializedName("location_id")
    private String locationId;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("deleted_at")
    private String deletedAt;

    private int logged_status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getForallLoc() {
        return forallLoc;
    }

    public void setForallLoc(String forallLoc) {
        this.forallLoc = forallLoc;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public int getLogged_status() {
        return logged_status;
    }

    public void setLogged_status(int logged_status) {
        this.logged_status = logged_status;
    }
}
