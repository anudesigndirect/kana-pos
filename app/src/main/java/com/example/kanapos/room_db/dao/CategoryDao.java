package com.example.kanapos.room_db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.pojo.CategoryPojo;
import com.example.kanapos.room_db.pojo.ProductPojo;

import java.util.List;

@Dao
public interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insert(List<CategoryPojo> category);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insertProduct(List<ProductPojo> category);


    @Query("SELECT * FROM " + DBConfig.table_categories+" ORDER BY priority ASC")
    List<CategoryPojo> getAllCategories();


    @Update
    int update(CategoryPojo category);


    @Delete
    void delete(CategoryPojo category);
}
