package com.example.kanapos.room_db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.pojo.CompanyPojo;

import java.util.List;

@Dao
public interface CompanyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(CompanyPojo companyModel);


    @Query("SELECT * FROM " + DBConfig.table_company)
    List<CompanyPojo> fetchAllCompanies();


    @Update
    void update(CompanyPojo companyModel);


    @Delete
    void delete(CompanyPojo companyModel);

}
