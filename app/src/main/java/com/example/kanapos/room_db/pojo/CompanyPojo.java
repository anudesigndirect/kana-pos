package com.example.kanapos.room_db.pojo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.kanapos.room_db.DBConfig;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity( tableName = DBConfig.table_company)
public class CompanyPojo {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("currency_id")
    @Expose
    private String currencyId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }
}
