package com.example.kanapos.room_db.pojo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.kanapos.room_db.DBConfig;

@Entity(tableName = DBConfig.order_methods)
public class OrderMethodsPojo {

    @PrimaryKey
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
