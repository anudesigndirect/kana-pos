package com.example.kanapos.room_db.dao;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.pojo.ProductPojo;

import java.util.List;

@Dao
public interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long[] insert(List<ProductPojo> products);


    @Query("SELECT * FROM " + DBConfig.table_products)
    List<ProductPojo> getAllData();

    @Query("SELECT * FROM " + DBConfig.table_products + " WHERE categoryId = :value")
    List<ProductPojo> getDataByCondition(String value);


    @Update
    int update(ProductPojo products);


    @Delete
    void delete(ProductPojo products);
}
