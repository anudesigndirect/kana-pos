package com.example.kanapos.network;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Api {

    String BASE_URL = "http://company.kanagoo.com/api/";

    @FormUrlEncoded
    @POST("register")
    Call<JsonElement> doLogin(@Field("email") String username, @Field("password") String password, @Field("key") String key);

    @FormUrlEncoded
    @POST("initialize")
    Call<JsonElement> location(@Field("key") String key, @Field("location_id") Integer location_id, @Field("license") String license, @Field("company_id") Integer company_id);
}
