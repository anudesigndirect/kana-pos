package com.example.kanapos.ui.dashboard;

import android.text.TextUtils;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.kanapos.room_db.pojo.ProductPojo;

public class ProductItemViewModel extends BaseObservable {
    private ProductPojo dataModel;

    public ProductItemViewModel(ProductPojo dataModel) {
        this.dataModel = dataModel;
    }

    public void setUp() {
        // perform set up tasks, such as adding listeners
    }

    public void tearDown() {
        // perform tear down tasks, such as removing listeners
    }

    @Bindable
    public String getTitle() {
        return !TextUtils.isEmpty(dataModel.getDisplayName()) ? dataModel.getDisplayName() : "";
    }
}