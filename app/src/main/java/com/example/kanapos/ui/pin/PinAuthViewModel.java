package com.example.kanapos.ui.pin;

import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.room_db.pojo.StaffPojo;

public class PinAuthViewModel extends BaseObservable {

    MutableLiveData<StaffPojo> mutable_staff = new MutableLiveData<>();
    MutableLiveData<String> logged_user = new MutableLiveData<>();
    MutableLiveData<String> pin_number = new MutableLiveData<>();
    MutableLiveData<String> notifications = new MutableLiveData<>();


    DBRepository dbRepository;

    public PinAuthViewModel(DBRepository dbRepository) {
        this.dbRepository = dbRepository;

        getLoggedStaff();
    }

    @Bindable
    public String getLoggedUser() {
        return logged_user.getValue();
    }

    @Bindable
    public String getPinNumber() {
        return pin_number.getValue();
    }

    public void getLoggedStaff() {

        new AsyncTask<Void, Void, StaffPojo>() {
            @Override
            protected StaffPojo doInBackground(Void... voids) {
                return dbRepository.getLoggedStaff();
            }

            @Override
            protected void onPostExecute(StaffPojo staffPojo) {
                super.onPostExecute(staffPojo);

                if (staffPojo != null) {
                    mutable_staff.setValue(staffPojo);
                    logged_user.setValue(staffPojo.getDisplayName());

                    notifyPropertyChanged(com.example.kanapos.BR.loggedUser);
                }
            }
        }.execute();
    }

    public void onClickKeys(View view) {

        TextView textView = (TextView) view;

        String existedValue = pin_number.getValue() == null ? "" : pin_number.getValue();

        if (existedValue.length() < 11) {
            /*Append textview value with pin.*/
            pin_number.setValue(existedValue + textView.getText().toString());

            notifyPropertyChanged(com.example.kanapos.BR.pinNumber);
        } else {

            notifications.setValue("Limit exceeded.");
        }

    }

    public void onClickBackButton() {
        String existedValue = pin_number.getValue();

        if (existedValue.length() > 0) {
            pin_number.setValue(existedValue.substring(0, existedValue.length() - 1));

            notifyPropertyChanged(com.example.kanapos.BR.pinNumber);
        }
    }


    public void onClickLogin() {

        StaffPojo logged_user = mutable_staff.getValue();

        if (pin_number.getValue().equals(logged_user.getPin())) {
            /*Success*/
            notifications.setValue("goToNext");
        } else {
            notifications.setValue("Wrong PIN");
        }
    }

}
