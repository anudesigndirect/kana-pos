package com.example.kanapos.ui.users;

import android.content.Context;
import android.os.AsyncTask;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.room_db.pojo.StaffPojo;

import java.util.ArrayList;
import java.util.List;

public class UserViewModel extends BaseObservable {
    private static final String TAG = "UserViewModel";
    private UserAdapter adapter;
    private List<StaffPojo> data;
    Context context;

    public UserViewModel(Context context) {
        this.context = context;
        data = new ArrayList<>();
        adapter = new UserAdapter(context);
    }

    public void setUp() {
        // perform set up tasks, such as adding listeners, data population, etc.
        getAllStaff();
    }

    public void tearDown() {
        // perform tear down tasks, such as removing listeners
    }

    @Bindable
    public List<StaffPojo> getData() {
        return this.data;
    }

    @Bindable
    public UserAdapter getAdapter() {
        return this.adapter;
    }

    public void getAllStaff() {

        new AsyncTask<String, String, List<StaffPojo>>() {
            @Override
            protected List<StaffPojo> doInBackground(String... strings) {
                return DBRepository.getInstance(context).getAllStaff();
            }

            @Override
            protected void onPostExecute(List<StaffPojo> staffPojos) {
                super.onPostExecute(staffPojos);

                data.clear();
                if (staffPojos != null) {
                    data.addAll(staffPojos);
                }
                notifyPropertyChanged(BR.data);
            }
        }.execute();

    }

}
