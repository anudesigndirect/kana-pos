package com.example.kanapos.ui.login;

import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.MutableLiveData;

import com.example.kanapos.utils.AppConstants;
import com.example.kanapos.network.Api;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginViewModel extends BaseObservable {
    private LoginModel user;

    private String successMessage = "Login was successful";
    private String errorMessage = "Email or Password not valid";

    MutableLiveData<LoginModel> mutable_login_response = new MutableLiveData<>();
    MutableLiveData<String> mutable_messages = new MutableLiveData<>();

    MutableLiveData<String> mutable_company = new MutableLiveData<>();
    MutableLiveData<Integer> progress_visibility;

    String androidID;

    public LoginViewModel(String androidID) {
        this.androidID = androidID;
        user = new LoginModel("", "");
    }

    @Bindable
    private String toastMessage = null;

    @Bindable
    public Integer getProgressVisibility() {

        if (progress_visibility == null) {
            progress_visibility = new MutableLiveData<>();
            progress_visibility.setValue(View.GONE);
        }
        return progress_visibility.getValue();
    }

    public void setUserEmail(String email) {
        user.setEmail(email);
        notifyPropertyChanged(com.example.kanapos.BR.userEmail);
    }

    @Bindable
    public String getUserEmail() {
        return user.getEmail();
    }

    @Bindable
    public String getUserPassword() {
        return user.getPassword();
    }

    public void setUserPassword(String password) {
        user.setPassword(password);
        notifyPropertyChanged(com.example.kanapos.BR.userPassword);
    }

    public void onLoginClicked() {
        if (isInputDataValid()) {
//            setToastMessage(successMessage);
            doLogin();
        } else {
            mutable_messages.setValue(errorMessage);
        }
//            setToastMessage(errorMessage);
    }

    public void onExitClicked() {
        mutable_messages.setValue("exist");
    }

    public boolean isInputDataValid() {
        return !TextUtils.isEmpty(getUserEmail()) && Patterns.EMAIL_ADDRESS.matcher(getUserEmail()).matches() && getUserPassword().length() > 5;
    }

    //This method is using Retrofit to get the JSON data from URL
    private void doLogin() {

        progress_visibility.setValue(View.VISIBLE);
        notifyPropertyChanged(BR.progressVisibility);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);

        Log.d("androiID", androidID + " a");

        Call<JsonElement> call = api.doLogin(getUserEmail(), getUserPassword(), androidID);

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    Log.d("Responseget", response.body() + " d");

                    LoginModel responseModel = new Gson().fromJson(response.body(), LoginModel.class);

                    mutable_login_response.setValue(responseModel);

                }
                progress_visibility.setValue(View.GONE);
                notifyPropertyChanged(BR.progressVisibility);

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.d("Responseget", t.getMessage() + " onFailure");
                progress_visibility.setValue(View.GONE);
                notifyPropertyChanged(BR.progressVisibility);
            }
        });
    }
}