package com.example.kanapos.ui.location_selector;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ActivityLocationBinding;
import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.shared_preferences.PrefConfig;
import com.example.kanapos.ui.users.UsersActivity;
import com.example.kanapos.utils.AppConstants;

public class LocationActivity extends AppCompatActivity {
    ActivityLocationBinding binding;

    LocationViewModel locationViewModel;

//    DBRepository dbRepository;

    Context context;

    PrefConfig prefConfig;
    static DBConfig DBConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_location);

        context = getApplicationContext();

        DBConfig = Room.databaseBuilder(context, DBConfig.class, DBRepository.DB_NAME).build();

//        dbRepository = DBRepository.getInstance(context);

        prefConfig = new PrefConfig(getApplicationContext());

        Integer location_id = getIntent().getExtras().getInt("location_id");
        Integer company_id = getIntent().getExtras().getInt("company_id");
        String company_name = getIntent().getExtras().getString("company_name");
        String token = getIntent().getStringExtra("token");
        String location_name = getIntent().getExtras().getString("location_name");
        String license = getIntent().getExtras().getString("license");

        locationViewModel = new LocationViewModel(location_id, license, company_id, token, AppConstants.getAndroidID(context));

        binding.setViewModel(locationViewModel);

        binding.executePendingBindings();

      /* locationViewModel. user.setCompany_name(name);
        notifyPropertyChanged(com.example.kanapos.BR.companyName);*/
        locationViewModel.setCompany(company_name);
        locationViewModel.setLocationName(location_name);

        locationViewModel.mutable_response.observe(this, new Observer<LocationModel>() {
            @Override
            public void onChanged(final LocationModel locationModel) {

                if (locationModel.getStaff() != null) {

                    /*Set as app already logged. No need to view login screen again after this.*/
                    prefConfig.insertBoolian(PrefConfig.APP_LOGIN_STATUS, true);


                    new AsyncTask<Void, Void, Long[]>() {
                        @Override
                        protected Long[] doInBackground(Void... voids) {
                            /*Save staff*/
                            DBConfig.staffDao().insert(locationModel.getStaff());
                            /*Save categories*/
                            DBConfig.categoryDao().insert(locationModel.getCategories());
                            /*Save products*/
                            DBConfig.productDao().insert(locationModel.getProducts());
                            /*Save button_colors*/
                            DBConfig.roomDao().insertButtonColor(locationModel.getButton_colors());
                            /*Save product positions*/
                            DBConfig.roomDao().insertProductPositions(locationModel.getProduct_positions());

                            return DBConfig.roomDao().insertOrderBillType(locationModel.getOrder_bill_types());
                        }

                        @Override
                        protected void onPostExecute(Long[] longs) {
                            super.onPostExecute(longs);

                        }
                    }.execute();

                    Intent intent = new Intent(context, UsersActivity.class);
                    startActivity(intent);
                }
            }
        });

        locationViewModel.mutable_messages.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {

                if (s != null) {

                    if (s.equals("exist")) {
                        finish();
                        System.exit(0);

                    } else {

                        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                   /* Snackbar snackbar = Snackbar
                            .make(binding.mainLayout, s, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorRed));
                    snackbar.show();*/
                    }
                }
            }
        });

    }
}
