package com.example.kanapos.ui.location_selector;

import android.util.Base64;
import android.util.Log;
import android.view.View;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.MutableLiveData;

import com.example.kanapos.utils.AppConstants;
import com.example.kanapos.network.Api;
import com.example.kanapos.ui.login.LoginModel;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LocationViewModel extends BaseObservable {
    private LoginModel user;

    Integer location_id;
    String license_key;
    Integer company_id;

    private String errorMessage = "Email or Password not valid";

    MutableLiveData<LocationModel> mutable_response = new MutableLiveData<>();
    MutableLiveData<String> mutable_messages = new MutableLiveData<>();
    MutableLiveData<Integer> progress_visibility;
    private String androidID;
    String token;

    public LocationViewModel(Integer location_id, String license_key, Integer company_id, String token, String androidID) {
        this.token = token;
        this.androidID = androidID;
        this.location_id = location_id;
        this.license_key = license_key;
        this.company_id = company_id;
        user = new LoginModel("", "");
    }

    @Bindable
    private String toastMessage = null;

    @Bindable
    public Integer getProgressVisibility() {

        if (progress_visibility == null) {
            progress_visibility = new MutableLiveData<>();
            progress_visibility.setValue(View.GONE);
            notifyPropertyChanged(BR.progressVisibility);
        }


        return progress_visibility.getValue();
    }

    public void setCompany(String name) {
        user.setCompany_name(name);
        notifyPropertyChanged(com.example.kanapos.BR.companyName);
    }

    @Bindable
    public String getCompanyName() {
        return user.getCompany_name();
    }

    @Bindable
    public String getLocationName() {
        return user.getLocation_name();
    }

    public void setLocationName(String name) {
        user.setLocation_name(name);
        notifyPropertyChanged(com.example.kanapos.BR.locationName);
    }

    public void onContinueClicked() {
        fetchData();

    }

    public void onExitClicked() {
        mutable_messages.setValue("exist");
    }


    //This method is using Retrofit to get the JSON data from URL
    private void fetchData() {

        progress_visibility.setValue(View.VISIBLE);
        notifyPropertyChanged(BR.progressVisibility);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);

        Log.d("DataPassing", androidID+" "+location_id + " d " + license_key + " " + company_id);

        Log.d("EncriptedValue", encrypt(decrypt()) + " enc");


        Call<JsonElement> call = api.location(androidID, location_id, encrypt(decrypt()), company_id);

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    Log.d("Responseget", response.body() + " d");

                    LocationModel responseModel = new Gson().fromJson(response.body(), LocationModel.class);

                    mutable_response.setValue(responseModel);

                }
                progress_visibility.setValue(View.GONE);
                notifyPropertyChanged(BR.progressVisibility);

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.d("Responseget", t.getMessage() + " onFailure");
                progress_visibility.setValue(View.GONE);
                notifyPropertyChanged(BR.progressVisibility);
            }
        });
    }


    /////////////////////////////////////////////////////////////////////////////////////////////

    public String decrypt() {
        try {
            IvParameterSpec iv = new IvParameterSpec(token.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(androidID.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/NOPADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decode(license_key, Base64.NO_WRAP));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();

            Log.d("DecryptValue", ex.getMessage() + " h");
        }

        return null;
    }

    public String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(token.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(token.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/NOPADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
