package com.example.kanapos.ui.dashboard;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ListCategoriesBinding;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.room_db.pojo.ButtonColorPojo;
import com.example.kanapos.room_db.pojo.CategoryPojo;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.DataViewHolder> {
    private static final String TAG = "CategoryAdapter";
    private List<CategoryPojo> data;
    Context context;
    DashBoardViewModel dashBoardViewModel;
    DBRepository dbRepository;

    TextView selected_textview;

    public CategoryAdapter(Context context, DashBoardViewModel dashBoardViewModel) {
        this.context = context;
        this.dashBoardViewModel = dashBoardViewModel;
        this.data = new ArrayList<>();
        dbRepository = DBRepository.getInstance(context);
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_categories,
                new FrameLayout(parent.getContext()), false);
        return new DataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        CategoryPojo dataModel = data.get(position);

        holder.categoryItemViewModel = new CategoryItemViewModel(dataModel);

        holder.setViewModel(holder.categoryItemViewModel);

        int button_color_id = data.get(position).getButtonColorId();

        if (position == 0) {
            /*Favorites*/
            holder.binding.tvCategory.setBackgroundResource(R.drawable.category_selected_bg);

            holder.item_view.performClick();
        } else {
//            holder.binding.tvCategory.setBackgroundResource(R.drawable.categories_bg);

            new ChangeButtonColor(holder.binding.tvCategory).execute(button_color_id);
        }


    }


    public class ChangeButtonColor extends AsyncTask<Integer, Void, ButtonColorPojo> {
        TextView textView;

        public ChangeButtonColor(TextView textView) {
            this.textView = textView;
        }

        @Override
        protected ButtonColorPojo doInBackground(Integer... integers) {
            return dbRepository.getButtonColorByID(integers[0]);
        }

        @Override
        protected void onPostExecute(ButtonColorPojo buttonColorPojo) {
            super.onPostExecute(buttonColorPojo);

            if (buttonColorPojo != null) {

                String hex = buttonColorPojo.getHex();
                Log.d("ButtonClor", hex + " d");

                if (hex != null) {
                    Drawable drawable = context.getResources().getDrawable(R.drawable.product_bg);
                    Log.d("ButtonClor", drawable.getClass().getName() + " db");

                    if (drawable instanceof ShapeDrawable) {
                        ((ShapeDrawable) drawable).getPaint().setColor(Color.parseColor(hex));
                    } else if (drawable instanceof GradientDrawable) {
                        Log.d("ButtonClor", hex + " shapedr d");
                        ((GradientDrawable) drawable).setColor(Color.parseColor(hex));
                    }

                    textView.setBackground(drawable);
                }
            }
        }
    }


    @Override
    public int getItemCount() {
        return this.data.size();
    }

    @Override
    public void onViewAttachedToWindow(DataViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(DataViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<CategoryPojo> data) {
        if (data == null || data.isEmpty()) {
            this.data.clear();
        } else {
            this.data.addAll(data);
        }
        notifyDataSetChanged();
    }

    class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ListCategoriesBinding binding;

        CategoryItemViewModel categoryItemViewModel;

        View item_view;

        DataViewHolder(View itemView) {
            super(itemView);
            bind();

            this.item_view = itemView;

            itemView.setOnClickListener(this);
        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind(); // Don't forget to unbind
            }
        }

        void setViewModel(CategoryItemViewModel viewModel) {
            if (binding != null) {
                binding.setViewModel(viewModel);
            }
        }

        @Override
        public void onClick(View view) {

           /* if (selected_textview != null) {

                //Change already selected categories color to back.
                selected_textview.setBackgroundResource(R.drawable.categories_bg);
            }

            selected_textview = binding.tvCategory;
            binding.tvCategory.setBackgroundResource(R.drawable.category_selected_bg);
*/
            if (getAdapterPosition() == 0) {
                /*Favorites*/
                dashBoardViewModel.getFavorites();
            } else {
                dashBoardViewModel.getProductsByCategory(data.get(getAdapterPosition()).getId() + "");
            }
        }
    }
}