package com.example.kanapos.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ActivityLoginBinding;
import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.ui.location_selector.LocationActivity;
import com.example.kanapos.utils.AppConstants;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding binding;

    LoginViewModel loginViewModel;

    DBRepository dbRepository;

    Context context;

    static DBConfig dbConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        context = getApplicationContext();

        dbConfig = Room.databaseBuilder(context, DBConfig.class, DBRepository.DB_NAME).build();

        dbRepository = DBRepository.getInstance(context);

        loginViewModel = new LoginViewModel(AppConstants.getAndroidID(context));

        binding.setViewModel(loginViewModel);

        binding.executePendingBindings();

        loginViewModel.mutable_login_response.observe(this, new Observer<LoginModel>() {
            @Override
            public void onChanged(final LoginModel locationModel) {
                Log.d("LocationGet", locationModel.getName() + " d");

                Intent intent = new Intent(context, LocationActivity.class);

                if (locationModel.getCompany() != null) {
                    new AsyncTask<Void, Void, Long>() {
                        @Override
                        protected Long doInBackground(Void... voids) {
                            return dbConfig.companyDao().insert(locationModel.getCompany());
                        }

                        @Override
                        protected void onPostExecute(Long longs) {
                            super.onPostExecute(longs);

                        }
                    }.execute();

                    intent.putExtra("company_name", locationModel.getCompany().getName());
                    intent.putExtra("token", locationModel.getCompany().getToken());
                    intent.putExtra("location_name", locationModel.getCompany().getName());
                    intent.putExtra("company_id", locationModel.getCompany().getId());
                }

                if (locationModel.getLocations() != null) {
                    if (locationModel.getLocations().size() > 0) {
                        intent.putExtra("location_id", locationModel.getLocations().get(0).getId());

                        new AsyncTask<Void, Void, Long>() {
                            @Override
                            protected Long doInBackground(Void... voids) {
                                return dbConfig.locationDao().insert(locationModel.getLocations().get(0));
                            }

                            @Override
                            protected void onPostExecute(Long longs) {
                                super.onPostExecute(longs);

                            }
                        }.execute();
                    }
                } else if (locationModel.getLocation() != null) {
                    {
                        intent.putExtra("location_id", locationModel.getLocation().getId());

                        new AsyncTask<Void, Void, Long>() {
                            @Override
                            protected Long doInBackground(Void... voids) {
                                return dbConfig.locationDao().insert(locationModel.getLocation());
                            }

                            @Override
                            protected void onPostExecute(Long longs) {
                                super.onPostExecute(longs);

                            }
                        }.execute();
                    }

                }
                intent.putExtra("license", locationModel.getLicenseKey());

                startActivity(intent);

            }
        });

        loginViewModel.mutable_messages.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {

                if (s != null) {

                    if (s.equals("exist")) {
                        finish();
                        System.exit(0);

                    } else {

                        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                   /* Snackbar snackbar = Snackbar
                            .make(binding.mainLayout, s, Snackbar.LENGTH_LONG);
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.colorRed));
                    snackbar.show();*/
                    }
                }
            }
        });

        binding.userEmail.setText("info@jaipuria.com");
        binding.userPassword.setText("admin123");
    }

    @BindingAdapter({"toastMessage"})
    public static void runMe(View view, String message) {
        if (message != null)
            Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
