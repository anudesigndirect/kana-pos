package com.example.kanapos.ui.dashboard;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.room.Room;

import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.room_db.pojo.CategoryPojo;
import com.example.kanapos.room_db.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.List;

public class DashBoardViewModel extends BaseObservable {
    private static final String TAG = "DashBoardViewModel";
    private final DBConfig dbConfig;
    private CategoryAdapter categoryAdapter;
    private List<CategoryPojo> categoryData;

    private ProductAdapter productAdapter;
    private List<ProductPojo> productData;
    Context context;

    public DashBoardViewModel(Context context) {
        this.context = context;
        categoryData = new ArrayList<>();
        categoryAdapter = new CategoryAdapter(context, this);
        productData = new ArrayList<>();
        productAdapter = new ProductAdapter(context);

        dbConfig = Room.databaseBuilder(context, DBConfig.class, DBRepository.DB_NAME).build();
    }

    public void setUp() {
        // perform set up tasks, such as adding listeners, categoryData population, etc.
        getAllCategories();

        getProductsByCategory("1");
    }

    public void tearDown() {
        // perform tear down tasks, such as removing listeners
    }

    @Bindable
    public List<CategoryPojo> getCategoryData() {
        return this.categoryData;
    }

    @Bindable
    public CategoryAdapter getCategoryAdapter() {
        return this.categoryAdapter;
    }

    @Bindable
    public List<ProductPojo> getProductData() {
        return this.productData;
    }

    @Bindable
    public ProductAdapter getProductAdapter() {
        return this.productAdapter;
    }

    public void getAllCategories() {

        new AsyncTask<String, String, List<CategoryPojo>>() {
            @Override
            protected List<CategoryPojo> doInBackground(String... strings) {
                return DBRepository.getInstance(context).getAllCategories();
            }

            @Override
            protected void onPostExecute(List<CategoryPojo> categoryPojo) {
                super.onPostExecute(categoryPojo);

                if (categoryPojo != null) {

                    /*Add Favorite*/
                    CategoryPojo pojo = new CategoryPojo();
                    pojo.setDisplayName("Favourites");
                    categoryPojo.add(0, pojo);

                    categoryData.clear();
                    categoryData.addAll(categoryPojo);
                    notifyPropertyChanged(BR.categoryData);
                }
            }
        }.execute();

    }

    public void getFavorites() {

        new AsyncTask<String, String, List<ProductPojo>>() {
            @Override
            protected List<ProductPojo> doInBackground(String... strings) {
                return dbConfig.roomDao().getFavorites();
            }

            @Override
            protected void onPostExecute(List<ProductPojo> productPojos) {
                super.onPostExecute(productPojos);
                Log.d("Favorites", productPojos + " d");

                productData.clear();
                if (productPojos != null) {
                    Log.d("Favorites", productPojos.size() + " d");

                    productData.addAll(productPojos);
                }

                notifyPropertyChanged(BR.productData);
            }
        }.execute();

    }

    public void getProductsByCategory(final String value) {

        new AsyncTask<String, String, List<ProductPojo>>() {
            @Override
            protected List<ProductPojo> doInBackground(String... strings) {
                return DBRepository.getInstance(context).getProductsByCategory(value);
            }

            @Override
            protected void onPostExecute(List<ProductPojo> productPojos) {
                super.onPostExecute(productPojos);

                productData.clear();
                if (productPojos != null) {

                    productData.addAll(productPojos);
                }

                notifyPropertyChanged(BR.productData);
            }
        }.execute();

    }

}
