package com.example.kanapos.ui.dashboard;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ListCategoriesBinding;
import com.example.kanapos.databinding.ListProductBinding;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.room_db.pojo.ButtonColorPojo;
import com.example.kanapos.room_db.pojo.ProductPojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.DataViewHolder> {
    private static final String TAG = "ProductAdapter";
    private List<ProductPojo> data;
    Context context;

    DBRepository dbRepository;


    public ProductAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        dbRepository = DBRepository.getInstance(context);


    }

    @Override
    public ProductAdapter.DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_product,
                new FrameLayout(parent.getContext()), false);
        return new ProductAdapter.DataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductAdapter.DataViewHolder holder, int position) {
        ProductPojo dataModel = data.get(position);
        holder.setViewModel(new ProductItemViewModel(dataModel));

        int button_color_id = data.get(position).getButtonColorId();
        new ChangeButtonColor(holder.binding.tvTitle).execute(button_color_id);

    }

    public class ChangeButtonColor extends AsyncTask<Integer, Void, ButtonColorPojo> {
        TextView textView;

        public ChangeButtonColor(TextView textView) {
            this.textView = textView;
        }

        @Override
        protected ButtonColorPojo doInBackground(Integer... integers) {
            return dbRepository.getButtonColorByID(integers[0]);
        }

        @Override
        protected void onPostExecute(ButtonColorPojo buttonColorPojo) {
            super.onPostExecute(buttonColorPojo);

            if (buttonColorPojo != null) {

                String hex = buttonColorPojo.getHex();
                Log.d("ButtonClor", hex + " d");

                if (hex != null) {
                    Drawable drawable = context.getResources().getDrawable(R.drawable.product_bg);
                    Log.d("ButtonClor", drawable.getClass().getName() + " db");

                    if (drawable instanceof ShapeDrawable) {
                        ((ShapeDrawable) drawable).getPaint().setColor(Color.parseColor(hex));
                    } else if (drawable instanceof GradientDrawable) {
                        Log.d("ButtonClor", hex + " shapedr d");
                        ((GradientDrawable) drawable).setColor(Color.parseColor(hex));
                    }

                    textView.setBackground(drawable);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    @Override
    public void onViewAttachedToWindow(ProductAdapter.DataViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(ProductAdapter.DataViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ProductPojo> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ListProductBinding binding;

        DataViewHolder(View itemView) {
            super(itemView);
            bind();

            itemView.setOnClickListener(this);
        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind(); // Don't forget to unbind
            }
        }

        void setViewModel(ProductItemViewModel viewModel) {
            if (binding != null) {
                binding.setViewModel(viewModel);
            }
        }

        @Override
        public void onClick(View view) {

        }
    }
}