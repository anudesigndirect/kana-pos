package com.example.kanapos.ui.pin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ActivityPinAuthBinding;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.ui.dashboard.DashboardActivity;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class PinAuthActivity extends AppCompatActivity {

    ActivityPinAuthBinding binding;

    PinAuthViewModel viewModel;

    DBRepository dbRepository;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_pin_auth);

        context = this;

        dbRepository = DBRepository.getInstance(context);

        viewModel = new PinAuthViewModel(dbRepository);

        binding.setViewModel(viewModel);

        binding.executePendingBindings();

        viewModel.notifications.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {

                if (s.equals("goToNext")) {

                    Intent intent = new Intent(context, DashboardActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                }
            }
        });

        String decrepted_value = decrypt("yOzUgC6Yr6FUepfGVzHajQ==");
        Log.d("DecryptValue", decrepted_value + " s");

        String encripted_value = encrypt(decrepted_value);
        Log.d("DecryptValue", encripted_value + " en");

    }

    private static final String key = "278BFBFF00300F21";
    private static final String token = "7QZ72BWZB8VZIMX6";

    public static String decrypt(String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(token.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/NOPADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decode(encrypted, Base64.NO_WRAP));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();

            Log.d("DecryptValue", ex.getMessage() + " h");
        }

        return null;
    }

    public static String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(token.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(token.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/NOPADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            return Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
