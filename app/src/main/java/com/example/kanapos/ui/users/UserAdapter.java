package com.example.kanapos.ui.users;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ListUsersBinding;
import com.example.kanapos.room_db.DBConfig;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.room_db.pojo.StaffPojo;
import com.example.kanapos.shared_preferences.PrefConfig;
import com.example.kanapos.ui.opening_balance.OpeningBalanceActivity;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.DataViewHolder> {
    private static final String TAG = "UserAdapter";
    private List<StaffPojo> data;
    Context context;
    static DBConfig DBConfig;

    public UserAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();

        DBConfig = Room.databaseBuilder(context, DBConfig.class, DBRepository.DB_NAME).build();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_users,
                new FrameLayout(parent.getContext()), false);
        return new DataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        StaffPojo dataModel = data.get(position);
        holder.setViewModel(new UserItemViewModel(dataModel));
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    @Override
    public void onViewAttachedToWindow(DataViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(DataViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<StaffPojo> data) {
        if (data == null || data.isEmpty()) {
            this.data.clear();
        } else {
            this.data.addAll(data);
        }
        notifyDataSetChanged();
    }

    class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ListUsersBinding binding;

        DataViewHolder(View itemView) {
            super(itemView);
            bind();

            itemView.setOnClickListener(this);
        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind(); // Don't forget to unbind
            }
        }

        void setViewModel(UserItemViewModel viewModel) {
            if (binding != null) {
                binding.setViewModel(viewModel);
            }
        }

        @Override
        public void onClick(View view) {

//            DBRepository db = DBRepository.getInstance(context);

            /*Change staff logged status*/
            final StaffPojo staffPojo = data.get(getAdapterPosition());
            staffPojo.setLogged_status(1);
//            db.updateStaff(staffPojo);


            new AsyncTask<Void, Void, Integer>() {
                @Override
                protected Integer doInBackground(Void... voids) {
                    return DBConfig.staffDao().update(staffPojo);
                }

                @Override
                protected void onPostExecute(Integer longs) {
                    super.onPostExecute(longs);

                    Log.d("DataGet", longs + " sd");
                }
            }.execute();

            PrefConfig prefConfig = new PrefConfig(context);
            prefConfig.insertBoolian(PrefConfig.USER_LOGGED_STATUS, true);

            Intent intent = new Intent(context, OpeningBalanceActivity.class);
            context.startActivity(intent);

        }
    }
}