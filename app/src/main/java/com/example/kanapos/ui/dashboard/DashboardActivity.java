package com.example.kanapos.ui.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ActivityDashboardBinding;
import com.example.kanapos.utils.SpacesItemDecoration;

public class DashboardActivity extends AppCompatActivity {

    ActivityDashboardBinding binding;
    DashBoardViewModel viewModel;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);

        context = this;

        viewModel = new DashBoardViewModel(context);

        binding.setViewModel(viewModel);

        binding.executePendingBindings();

        initRecyclerView(binding.getRoot());

        int width=getScreenWidth(this);

        binding.line1.setMinimumWidth(width);
        binding.include1.line2.setMinimumWidth(width);

        binding.include1.edtQuantity.setRawInputType(InputType.TYPE_CLASS_NUMBER);


        viewModel.setUp();

    }
    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        viewModel.tearDown();
    }


    private void initRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.rec_category);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.item_gap);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));


        RecyclerView rec_product = view.findViewById(R.id.rec_product);
        rec_product.setLayoutManager(new GridLayoutManager(context,2));
//        rec_product.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
    }
}