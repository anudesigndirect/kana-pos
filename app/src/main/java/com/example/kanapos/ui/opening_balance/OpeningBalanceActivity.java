package com.example.kanapos.ui.opening_balance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ActivityOpeningBalanceBinding;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.ui.pin.PinAuthActivity;

public class OpeningBalanceActivity extends AppCompatActivity {
    Context context;

    ActivityOpeningBalanceBinding binding;

    OpeningBalanceViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_opening_balance);

        context = this;

        viewModel = new OpeningBalanceViewModel(DBRepository.getInstance(context));

        binding.setViewModel(viewModel);

        binding.executePendingBindings();

        viewModel.setOpening_balance("0");

        binding.etAmount.setSelection(binding.etAmount.getText().toString().length());


        viewModel.notifications.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {

                if (s.equals("goToNext")) {

                    Intent intent = new Intent(context, PinAuthActivity.class);
                    startActivity(intent);

                } else {
                    Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}
