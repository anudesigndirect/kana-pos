package com.example.kanapos.ui.splash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ActivitySplashBinding;
import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.room_db.pojo.OpeningBalancePojo;
import com.example.kanapos.shared_preferences.PrefConfig;
import com.example.kanapos.ui.login.LoginActivity;
import com.example.kanapos.ui.opening_balance.OpeningBalanceActivity;
import com.example.kanapos.ui.pin.PinAuthActivity;
import com.example.kanapos.ui.users.UsersActivity;
import com.example.kanapos.utils.AppUtil;

import java.util.Date;

public class SplashActivity extends AppCompatActivity {

    ActivitySplashBinding binding;

    PrefConfig prefConfig;

    private Context context;

    DBRepository dbRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        context = getApplicationContext();

        dbRepository = DBRepository.getInstance(context);

        prefConfig = new PrefConfig(context);

        doProgressAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    int index = 1;

    public void doProgressAnimation() {


        binding.progress.setProgress(index);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (index < 100) {
                    index += 1;
                    doProgressAnimation();
                } else {

                    if (prefConfig.isAppLogged()) {

                        if (prefConfig.isUserLogged()) {
                            /*User already logged in*/

                            final String today = AppUtil.yyyyMMdd.format(new Date());

                            new AsyncTask<Void, Void, OpeningBalancePojo>() {
                                @Override
                                protected OpeningBalancePojo doInBackground(Void... voids) {
                                    return dbRepository.getOpeningBalancebyDate(today);
                                }

                                @Override
                                protected void onPostExecute(OpeningBalancePojo openingBalance) {
                                    super.onPostExecute(openingBalance);

                                    if (openingBalance != null) {
                                        /*Go to Pin*/
                                        Intent intent = new Intent(context, PinAuthActivity.class);

                                        startActivity(intent);
                                    } else {
                                        /*Show opening balance window*/
                                        Intent intent = new Intent(context, OpeningBalanceActivity.class);

                                        startActivity(intent);
                                    }
                                }
                            }.execute();

                        } else {
                            /*Go to user selection page*/
                            Intent intent = new Intent(context, UsersActivity.class);

                            startActivity(intent);
                        }
                    } else {
                        /*Go to login screen*/
                        Intent intent = new Intent(context, LoginActivity.class);

                        startActivity(intent);
                    }
                }
            }
        }, 0);

    }
}
