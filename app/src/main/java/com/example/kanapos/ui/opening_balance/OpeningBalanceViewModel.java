package com.example.kanapos.ui.opening_balance;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.example.kanapos.room_db.DBRepository;
import com.example.kanapos.room_db.pojo.OpeningBalancePojo;
import com.example.kanapos.utils.AppUtil;

import java.util.Date;

public class OpeningBalanceViewModel extends BaseObservable {
    OpeningBalancePojo openingBalancePojo;

    MutableLiveData<String> notifications = new MutableLiveData<>();

    DBRepository dbRepository;

    public OpeningBalanceViewModel(DBRepository dbRepository) {
        this.dbRepository = dbRepository;
        openingBalancePojo = new OpeningBalancePojo(0, "");
    }

    public void setOpening_balance(String amount) {
        openingBalancePojo.setAmount(Double.parseDouble(amount));
        notifyPropertyChanged(com.example.kanapos.BR.openingBalance);
    }

    @Bindable
    public String getOpeningBalance() {
        return openingBalancePojo.getAmount() + "";
    }

    public void onButtonClicked() {
        if (getOpeningBalance().equals("")) {
            notifications.setValue("Enter opening balance.");
        } else {

//            OpeningBalancePojo openingBalance = new OpeningBalancePojo();
            openingBalancePojo.setAmount(Double.parseDouble(getOpeningBalance()));
            openingBalancePojo.setDate_at(AppUtil.yyyyMMdd.format(new Date()));
            dbRepository.insertOpeningBalance(openingBalancePojo);

            notifications.setValue("goToNext");
        }
    }

    public void insertOpeningBalance() {

    }

}
