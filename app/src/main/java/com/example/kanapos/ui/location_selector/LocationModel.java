package com.example.kanapos.ui.location_selector;

import com.example.kanapos.room_db.pojo.ButtonColorPojo;
import com.example.kanapos.room_db.pojo.CategoryPojo;
import com.example.kanapos.room_db.pojo.OrderBillTypesPojo;
import com.example.kanapos.room_db.pojo.ProductPojo;
import com.example.kanapos.room_db.pojo.ProductPositionsPojo;
import com.example.kanapos.room_db.pojo.StaffPojo;

import java.util.List;

public class LocationModel {

    private String location_main;
    private String location_sub;

    List<StaffPojo> staff;
    List<CategoryPojo> categories;
    List<ProductPojo> products;
    List<ButtonColorPojo> button_colors;
    List<ProductPositionsPojo> product_positions;
    List<OrderBillTypesPojo> order_bill_types;

    public LocationModel(String location_main, String location_sub) {
        this.location_main = location_main;
        this.location_sub = location_sub;
    }

    public List<ProductPositionsPojo> getProduct_positions() {
        return product_positions;
    }

    public void setProduct_positions(List<ProductPositionsPojo> product_positions) {
        this.product_positions = product_positions;
    }

    public List<ButtonColorPojo> getButton_colors() {
        return button_colors;
    }

    public void setButton_colors(List<ButtonColorPojo> button_colors) {
        this.button_colors = button_colors;
    }

    public List<OrderBillTypesPojo> getOrder_bill_types() {
        return order_bill_types;
    }

    public void setOrder_bill_types(List<OrderBillTypesPojo> order_bill_types) {
        this.order_bill_types = order_bill_types;
    }

    public String getLocation_main() {
        return location_main;
    }

    public List<StaffPojo> getStaff() {
        return staff;
    }

    public List<CategoryPojo> getCategories() {
        return categories;
    }

    public List<ProductPojo> getProducts() {
        return products;
    }

    public void setProducts(List<ProductPojo> products) {
        this.products = products;
    }

    public void setCategories(List<CategoryPojo> categories) {
        this.categories = categories;
    }

    public void setStaff(List<StaffPojo> staff) {
        this.staff = staff;
    }

    public void setLocation_main(String location_main) {
        this.location_main = location_main;
    }

    public String getLocation_sub() {
        return location_sub;
    }

    public void setLocation_sub(String location_sub) {
        this.location_sub = location_sub;
    }
}
