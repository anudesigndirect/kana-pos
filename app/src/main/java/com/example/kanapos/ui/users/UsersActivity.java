package com.example.kanapos.ui.users;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.example.kanapos.R;
import com.example.kanapos.databinding.ActivityUsersBinding;
import com.example.kanapos.shared_preferences.PrefConfig;
import com.example.kanapos.utils.SpacesItemDecoration;

import static android.widget.LinearLayout.VERTICAL;

public class UsersActivity extends AppCompatActivity {

    private UserViewModel userViewModel;

    PrefConfig prefConfig;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        View view = bind();
        initRecyclerView(view);

        prefConfig = new PrefConfig(getApplicationContext());

        userViewModel.setUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        userViewModel.tearDown();
    }

    private View bind() {
        ActivityUsersBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_users);
        userViewModel = new UserViewModel(context);
        binding.setViewModel(userViewModel);
        return binding.getRoot();
    }

    private void initRecyclerView(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.user_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), 1));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.margin_top);
        recyclerView.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
    }
}
