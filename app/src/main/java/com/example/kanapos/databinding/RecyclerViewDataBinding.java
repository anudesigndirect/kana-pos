package com.example.kanapos.databinding;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kanapos.room_db.pojo.CategoryPojo;
import com.example.kanapos.room_db.pojo.ProductPojo;
import com.example.kanapos.room_db.pojo.StaffPojo;
import com.example.kanapos.ui.dashboard.CategoryAdapter;
import com.example.kanapos.ui.dashboard.ProductAdapter;
import com.example.kanapos.ui.users.UserAdapter;

import java.util.List;

public class RecyclerViewDataBinding {
    /**
     * Binds the data to the {@link android.support.v7.widget.RecyclerView.Adapter}, sets the
     * recycler view on the adapter, and performs some other recycler view initialization.
     *
     * @param recyclerView passed in automatically with the data binding
     * @param adapter      must be explicitly passed in
     * @param data         must be explicitly passed in
     */
    @BindingAdapter({"app:adapter", "app:data"})
    public void bind(RecyclerView recyclerView, UserAdapter adapter, List<StaffPojo> data) {
        recyclerView.setAdapter(adapter);
        adapter.updateData(data);
    }

    @BindingAdapter({"app:adapter", "app:data"})
    public void bind(RecyclerView recyclerView, CategoryAdapter adapter, List<CategoryPojo> data) {
        recyclerView.setAdapter(adapter);
        adapter.updateData(data);
    }

    @BindingAdapter({"app:adapter", "app:data"})
    public void bind(RecyclerView recyclerView, ProductAdapter adapter, List<ProductPojo> data) {
        recyclerView.setAdapter(adapter);
        adapter.updateData(data);
    }
}
