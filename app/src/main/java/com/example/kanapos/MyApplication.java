package com.example.kanapos;

import android.app.Application;

//import android.databinding.DataBindingUtil;

import androidx.databinding.DataBindingUtil;

import com.example.kanapos.databinding.AppDataBindingComponent;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        DataBindingUtil.setDefaultComponent(new AppDataBindingComponent());
    }
}
